import { LightningElement, track } from 'lwc';
export default class HelloWorld extends LightningElement {
    @track greeting = 'universe merry go round from git 1 user frm vscode';
  
    changeHandler(event) {
        this.greeting = event.target.value;
    }
}